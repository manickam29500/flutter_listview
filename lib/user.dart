import 'package:flutter/material.dart';

List<dynamic> users = [
  {
    "employee_name": 'James Robert',
    "employee_id": '100',
    "designation": 'Senior Engineer',
    "blood_group": 'B+',
  },
  {
    "employee_name": 'James Anderson',
    "employee_id": '101',
    "designation": 'Senior DevOps',
    "blood_group": 'B+',
  },
  {
    "employee_name": 'David Charles',
    "employee_id": '103',
    "designation": 'Junior Developer',
    "blood_group": 'B+',
  },
  {
    "employee_name": 'Charles Joseph',
    "employee_id": '104',
    "designation": 'Senior Developer',
    "blood_group": 'A+',
  },
  {
    "employee_name": 'Donald Steven',
    "employee_id": '105',
    "designation": 'Senior Tester',
    "blood_group": 'A+',
  },
  {
    "employee_name": 'Steven Rogers',
    "employee_id": '106',
    "designation": 'Senior DevOps',
    "blood_group": 'A+',
  },
  {
    "employee_name": 'Charles Joseph',
    "employee_id": '107',
    "designation": 'Senior Developer',
    "blood_group": 'A+',
  },
  {
    "employee_name": 'Kevin Paul',
    "employee_id": '108',
    "designation": 'Senior Developer',
    "blood_group": 'A-',
  },
  {
    "employee_name": 'George Lang',
    "employee_id": '109',
    "designation": 'Software Developer',
    "blood_group": 'B+',
  },
  {
    "employee_name": 'Justin Lang',
    "employee_id": '110',
    "designation": 'Senior Developer',
    "blood_group": 'A+',
  },
];
