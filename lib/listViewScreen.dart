import 'package:flutter/material.dart';
import 'package:flutter_listview/user.dart';

class ListViewScreen extends StatelessWidget {
  const ListViewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
            itemCount: users.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 100,
                margin: EdgeInsets.all(1),
                child: ListTile(
                  leading: Icon(Icons.account_circle,size: 40,),
                  title: Text(users[index]['employee_name'],style: TextStyle(fontSize: 20),),
                  subtitle: Text(users[index]['designation'],style: TextStyle(fontSize: 20),),
                  trailing: Icon(Icons.phone,size: 30,),
                )
              );
            }));
  }
}
